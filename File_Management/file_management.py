


def main():
    # write
    f = open('docker_commands.txt', mode='wt', encoding='utf-8')
    f.write('This is a sample line added to the docker commands file')
    f.write('This is another line....\n')
    f.write('and a last one.')
    f.close()

    #read
    g = open('docker_commands.txt', mode='rt', encoding='utf-8')
    g.read(32)
    g.read()
    g.seek(0)
    g.readline()
    g.readlines()
    g.close()

    # append text
    h = open('docker_commands.txt', mode='at', encoding='utf-8')
    h.writelines(
        ['This is a sample line added to the docker commands file,\n',
         'This is another line... ',
         'and a last one.']

    )
    h.close()



if __name__ == '__main__':
    main()