from contextlib import  closing

class RefrigeratorRaider:

    def open(self):
        print('Opening fridge door')

    def take(self, food):
        print("Finding {}...".format(food))
        if food == 'deep fried pizza':
            raise RuntimeError('Health Warning!')
        print("Taking {}".format(food))

    def close(self):
        print('Close fridge door')


def raid(food):
    with closing(RefrigeratorRaider()) as r:
        r.open()
        r.take(food)

    # r = RefrigeratorRaider()
    # r.open()
    # r.take(food)
    # r.close()