def encode_resistor_colors(ohms_string):
    """Find the color encoding from an ohms string"""
    colors_string = ''
    clean_ohms_string = ''
    order_discriminant = 3

    if 'M' in ohms_string:
        order = 1000000
        initial_ohms_string = ohms_string[:-len('M ohms')]
        true_value = int(float(initial_ohms_string) * order)
        true_value_length = len(str(true_value))
        power_of_ten = find_power_of_ten(true_value_length, order_discriminant)
        clean_ohms_string = str(true_value)[:-power_of_ten]
        clean_ohms_string += str(power_of_ten)

    elif 'k' in ohms_string:
        order = 1000
        initial_ohms_string = ohms_string[:-len('k ohms')]
        true_value = int(float(initial_ohms_string) * order)
        true_value_length = len(str(true_value))
        power_of_ten = find_power_of_ten(true_value_length, order_discriminant)
        clean_ohms_string = str(true_value)[:-power_of_ten]
        clean_ohms_string += str(power_of_ten)

    else:
        clean_ohms_string = ohms_string[:-len(' ohms')]
        power_of_ten = find_power_of_ten(len(clean_ohms_string), order_discriminant)

        if power_of_ten > 0:
            clean_ohms_string = clean_ohms_string[:-power_of_ten]

        clean_ohms_string += str(power_of_ten)

    for index, color_char in enumerate(clean_ohms_string):
        if index == 0:
            colors_string += '{}'.format(get_color_string(color_char))
        else:
            colors_string += ' {}'.format(get_color_string(color_char))

    colors_string += ' gold'

    return colors_string


def find_power_of_ten(true_value_length, order_discriminant):
    """Return the corresponding power of ten"""
    return true_value_length + 1 - order_discriminant;


def get_color_string(color_value):
    """Returns string for passed color value"""
    color_list = ['black', 'brown', 'red', 'orange', 'yellow', 'green', 'blue', 'violet', 'gray', 'white']
    return color_list[int(color_value)]


test.describe("Basic tests")
test.it("Some common resistor values")
test.assert_equals(encode_resistor_colors("10 ohms"), "brown black black gold")
test.assert_equals(encode_resistor_colors("47 ohms"), "yellow violet black gold")
test.assert_equals(encode_resistor_colors("100 ohms"), "brown black brown gold")
test.assert_equals(encode_resistor_colors("220 ohms"), "red red brown gold")
test.assert_equals(encode_resistor_colors("330 ohms"), "orange orange brown gold")
test.assert_equals(encode_resistor_colors("470 ohms"), "yellow violet brown gold")
test.assert_equals(encode_resistor_colors("680 ohms"), "blue gray brown gold")
test.assert_equals(encode_resistor_colors("1k ohms"), "brown black red gold")
test.assert_equals(encode_resistor_colors("4.7k ohms"), "yellow violet red gold")
test.assert_equals(encode_resistor_colors("10k ohms"), "brown black orange gold")
test.assert_equals(encode_resistor_colors("22k ohms"), "red red orange gold")
test.assert_equals(encode_resistor_colors("47k ohms"), "yellow violet orange gold")
test.assert_equals(encode_resistor_colors("100k ohms"), "brown black yellow gold")
test.assert_equals(encode_resistor_colors("330k ohms"), "orange orange yellow gold")
test.assert_equals(encode_resistor_colors("1M ohms"), "brown black green gold")
test.assert_equals(encode_resistor_colors("2M ohms"), "red black green gold")

# ===================================================================================================================

import itertools
import numpy as np


def count_change(money, coins):
    ranges_list = [list(range(int(money / coin) + 1)) for coin in coins]
    combinations = list(itertools.product(*ranges_list))
    combination_matrix = np.array(combinations)
    solutions = np.dot(combination_matrix, coins)

    return list(solutions).count(money)


import unittest


# Note: the class must be called Test
class Test(unittest.TestCase):
    def test_should_handle_the_example_case(self):
        self.assertEqual(count_change(4, [1, 2]), 3)

    def test_should_handle_another_simple_case(self):
        self.assertEqual(count_change(10, [5, 2, 3]), 4)

    def test_should_handle_a_case_with_no_possible_combinations(self):
        self.assertEqual(count_change(301, [5, 10, 20, 50, 100, 200, 500]), 0)

    def test_should_handle_no_change_given(self):
        self.assertEqual(count_change(11, [5, 7]), 0)

    def test_should_handle_some_tougher_cases(self):
        self.assertEqual(count_change(199, [3, 5, 9, 15]), 760)

    def test_should_handle_the_coins_regardless_of_order(self):
        self.assertEqual(count_change(300, [5, 10, 20, 50, 100, 200, 500]), 1022)

# ===================================================================================================================

def minimum_swaps(ratings):
    number_of_steps = 0
    ordered_ratings = ratings.copy()

    for rating_index in range(len(ratings)):
        rating = ordered_ratings[rating_index]
        remaining_to_order = ordered_ratings[rating_index:]
        max_rating = max(remaining_to_order)

        if rating == max_rating:
            continue
        else:
            number_of_steps += 1
            max_index = remaining_to_order.index(max_rating) + len(ordered_ratings[:rating_index])
            ordered_ratings[rating_index], ordered_ratings[max_index] = ordered_ratings[max_index], ordered_ratings[
                rating_index]

    return number_of_steps


import unittest


class Test(unittest.TestCase):
    def test_minimum_swaps_should_handle_multiple_swaps(self):
        self.assertEqual(minimum_swaps([5, 3, 1, 2, 4]), 2)

    def test_minimum_swaps_should_handle_single_swap(self):
        self.assertEqual(minimum_swaps([3, 1, 2]), 1)

    def test_minimum_swaps_should_handle_single_swap_2(self):  # [4, 2, 3, 1] [5, 4, 2, 3, 1]
        self.assertEqual(minimum_swaps([5, 4, 2, 3, 1]), 1)

    def test_minimum_swaps_should_handle_multiple_swaps(self):
        self.assertEqual(minimum_swaps([4, 5, 1, 2, 3]), 2)

    def test_minimum_swaps_should_handle_multiple_swaps_in_long_list(self):
        self.assertEqual(minimum_swaps(
            [1, 12, 3, 4, 5, 18, 6, 7, 8, 9, 10, 11, 13, 2, 14, 16, 17, 15, 18, 19, 20, 22, 21, 25, 24, 23, 26, 29, 28,
             27, 30, 35, 31, 32, 34, 36, 38, 37, 39, 40]), 31)

    def test_minimum_swaps_should_handle_multiple_swaps_in_long_list_2(self):
        self.assertEqual(minimum_swaps(
            [35, 1, 12, 3, 4, 5, 18, 6, 7, 8, 27, 9, 10, 11, 13, 2, 14, 23, 16, 17, 15, 18, 19, 20, 22, 21, 25, 24, 26,
             29, 28, 30, 31, 32, 34, 36, 38, 37, 39, 40, 45, 44, 41, 42, 46, 43, 48, 47, 49, 51, 52, 53, 54, 57, 56,
             55]), 52)

    def test_ascending_list(self):
        self.assertEqual(minimum_swaps([1, 12, 3, 4]), 2)

# ===================================================================================================================


import math


def sol_equa(n):
    """ Returns the list of solutions to a diophantine equation of the form x ^ 2 - 4 * y ^ 2 = n"""
    results = []
    for i in range(1, int(math.sqrt(n)) + 1):
        # i + j = x - 2y + x + 2y --> i + j = 2x --> (i + j) / 2 = x
        # j - i = x + 2y - (x - 2y) --> j - i = 4y --> (j - i) / 4 = y
        if n % i == 0:
            j = n // i
            if (i + j) % 2 == 0 and (j - i) % 4 == 0:
                x = (i + j) // 2
                y = (j - i) // 4
                results.append([x, y])

    return results

test.assert_equals(sol_equa(12), [[4, 1]])
test.assert_equals(sol_equa(13), [[7, 3]])
test.assert_equals(sol_equa(16), [[4, 0]])
test.assert_equals(sol_equa(17), [[9, 4]])