import unittest
import os


def analyze_text(filename):
    lines = 0
    chars = 0
    with open(filename, 'r') as f:
        for line in f:
            lines += 1
            chars += len(line)

    return lines, chars


class TextAnalisisTests(unittest.TestCase):

    def setUp(self):
        '''Fixture that creates a file for the text methods to use.'''

        self.filename = 'text_analysis_test_file.txt'
        with open(self.filename, 'w') as f:
            f.write('Now we are engaged in a great cilvil war.\n'
                    'testing whether that nation,\n'
                    'or any nation so convinced and so dedicated,\n'
                    'can long endure')

    def tearDown(self):
        '''Fixtures that deletes the files used by the test methods'''
        try:
            os.remove(self.filename)
        except:
            pass

    def test_function_runs(self):
        """Basic smoke test: Does the function run."""
        analyze_text(self.filename)

    def test_line_count(self):
        self.assertEqual(analyze_text(self.filename)[0], 4)

    def test_character_count(self):
        self.assertEqual(analyze_text(self.filename)[1], 131)

    def test_no_such_file(self):
        '''Check that the proper exception is thrown for a missing file'''
        with self.assertRaises(IOError):
            analyze_text('foobar')

    def test_no_deletion(self):
        '''Check that the function does not delete the input file.'''
        analyze_text(self.filename)
        self.assertTrue(os.path.exists(self.filename))


if __name__ == '__main__':
    unittest.main()