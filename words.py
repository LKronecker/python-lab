import sys
from urllib.request import urlopen


def fetch_words(url):  # 'http://sixty-north.com/c/t.txt'
    with urlopen(url) as story:
        story_words = []
        for line in story:
            line_words = line.decode('utf-8').split()
            for word in line_words:
                story_words.append(word)

    return story_words


def print_items(items):
    for item in items:
        print(item)


def main(url):
    words = fetch_words(url)
    print_items(words)


# if the __name__ is equal to __main__ then we are in the case in which the module is being run as a script (file.py).
# run the code that is necessary to conform to the scrip context in that case
if __name__ == '__main__':
    main(sys.argv[1]) # the 0th arr=g of sys.argv is the module filename